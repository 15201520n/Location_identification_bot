import { API } from './API.js';
import { creating_request } from './creating_request.js';

export class APIFoursquare extends API {
    api = 'api.foursquare.com/v3/'
    request_parametrs

    async getplacesearch(request_data) {
        this.options.method = 'GET'
        this.request_parametrs = 'places/search?'
        let response;
        try {
            response = await fetch(`https://${this.api}${this.request_parametrs}${creating_request(request_data) }`, this.options)
        } catch (err) {
            alert(err.message)
        }
        
        const returnJ = await response.json()
        return JSON.stringify(returnJ);
    }
}