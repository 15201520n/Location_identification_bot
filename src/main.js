import { APIFoursquare } from './APIFoursquare.js'
import { getRequestParameters } from './data.js'

const SATNAV_INACCURACY = 8;

function getMinDistanceIndex(APIdata) {
    const distances = APIdata.results.map(result => result.distance);
    return distances.indexOf(Math.min(...distances));
}

function filterResults(APIdata, ...excluded) {
    const min_distance_index = getMinDistanceIndex(APIdata);
    return APIdata.results.filter(result =>
        result.distance < (APIdata.results[min_distance_index].distance + SATNAV_INACCURACY)
        && !excluded.includes((result.categories[0].id - (result.categories[0].id % 1000))));
}

function checkTime(place, time, day) {
    return place?.hours_popular[day]?.close > place?.hours_popular[day]?.open
        ? place?.hours_popular[day]?.close > time && time > place?.hours_popular[day]?.open
        : time > place?.hours_popular[day]?.opentime
            ? true
            : time < place?.hours_popular[day]?.close;
}

function getPlaceScores(filtered_results, time, day) {
    return filtered_results.map(place => {
        let rate;
        if (place?.hours_popular) {
            rate = (checkTime(place, time, day)) ? 0.3 : 0
        }
        else rate = 0;
        rate += Math.min(...filtered_results.map(_place => _place.price)) > place.price ? 0.1 : 0;
        rate += Math.min(...filtered_results.map(_place => _place.popularity)) > place.popularity ? 0.2 : 0;
        return rate;
    });
}

function findNumberOfMaxElement(grade) {
    return grade.indexOf(Math.max(...grade));
}

async function findSuitablePlace(objectdatarequest) {
    const objectAPI = new APIFoursquare();
    const today = new Date();
    const time = today.getHours();
    const day = today.getDay();
    const response = await objectAPI.getplacesearch(objectdatarequest);
    const APIdata = JSON.parse(response);
    const possibleValues = filterResults(APIdata, 18000);
    const grade = getPlaceScores(possibleValues, time * 100, day);
    const maxnumber = findNumberOfMaxElement(grade);
    
    document.getElementById("answer").value = `Name: ${possibleValues[maxnumber].name} Address: ${possibleValues[maxnumber].location.address}`;
}

async function searchButtonClick(event) {
    event.preventDefault();
    const params = getRequestParameters(mainForm)
    findSuitablePlace(params);
}

const mainForm = document.getElementById('mainFormId')
document.getElementById('search').addEventListener('click', searchButtonClick)