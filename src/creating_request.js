export function creating_request(request_data) {
    return Object.keys(request_data).map(prop => `${prop}=${Array.isArray(request_data[prop])
        ? request_data[prop].join('%2C') : request_data[prop]}&`).join('');
}


