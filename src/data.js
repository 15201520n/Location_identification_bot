
export function getRequestParameters(form) {
    const formData = new FormData(form);
    const values = Object.fromEntries(formData.entries());

    let objectdatarequest = {
        categories: [values.categories],
        ll: [values.longtitude, values.latitude],
        radius: values.radius,
        open_now: true,
        fields: [
            'categories',
            'name',
            'location',
            'rating',
            'popularity',
            'price',
            'hours_popular',
            'hours',
            'tastes',
            'features',
            'distance',
            'related_places'
        ]
    }
    return objectdatarequest;
}