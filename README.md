Location identification bot
====
Location identification bot is a program that returns the name and address of the user's places based on their coordinates. It calculates the place where the user can be located not only by his distance to the nearest places, but also taking into account a number of other parameters, such as: rating, popularity, hours of activity and user can to change the categories of places which the building belongs to a more accurate definition.<br>
Input data: Coordinates, location search radius, categories to which the places belong<br>
Output data: address of the place and its name
